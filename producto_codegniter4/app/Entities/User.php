<?php 

namespace App\Entities;
use CodeIgniter\Entity;

class User extends Entity {
	private $id_producto;
	private $nombre_producto;
	private $precio;
	private $stock;
	private $id_proveedor;
	private $id_marca;
	private $id_categoria;

	public function getIdProducto(){
       return $this->id_producto;

    }

	 public function setIdProducto($id_producto){
        $this->id_producto = $id_producto;
    }
    public function getNombreproducto(){
       return $this->nombre_producto;
    }

     public function setNombreproducto($nombre_producto){
        $this->nombre_producto = $nombre_producto;
    }
     public function getPrecio(){
       return $this->precio;
    }
     public function setPrecio($precio){
        $this->precio = $precio;
    }
    public function getStock(){
       return $this->stock;
    }
    public function setStock($stock){
        $this->stock = $stock;
    }
     public function getProveedor(){
       return $this->id_proveedor;
    }

    public function setIdProveedor($id_proveedor){
        $this->id_proveedor = $id_proveedor;
    }
    public function getMarca(){
       return $this->id_marca;
    }
     public function setIdMarca($id_marca){
        $this->id_marca = $id_marca;
    }
    public function getCategoria(){
       return $this->id_categoria;
    }

    public function setIdCategoria($id_categoria){
        $this->id_categoria = $id_categoria;
    }

}