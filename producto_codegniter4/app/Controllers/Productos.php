<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Entities\User;
use App\Models\Producto;

class Productos extends Controller
{
	public function index(){
		$producto = new Producto();

		$data['productos'] = $producto->getProductos();
		$data['marca'] =  $producto->getMarca();
		$data['proveedor'] = $producto->getProveedor();
		$data['categoria'] = $producto->getCategoria();



		echo view('templates/header', $data);
		echo view('producto/overview');
		echo view('templates/footer');

	   }

	   public function delete($id){
		$producto = new Producto();

		$msj = $producto->eliminar($id);

		if ($msj == true) {
			echo "<script>alert('Eliminado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Eliminar!!!')</script>";
		}

		return redirect()->to('/producto_codegniter4/public/Productos');
	}

	public function insertar()
	{
		$producto = new Producto();

		// Create
		$user = new User();
	    $user->setNombreproducto($this->request->getPost('nombre_producto'));
	    $user->setPrecio($this->request->getPost('precio'));
	    $user->setStock($this->request->getPost('stock'));
	    $user->setIdProveedor($this->request->getPost('proveedor'));
	    $user->setIdMarca($this->request->getPost('marca'));
	     $user->setIdCategoria($this->request->getPost('categoria'));

       $msj = $producto->insertar($user);

		if ($msj == "add") {
			echo "<script>alert('Agregado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Agregar!!!')</script>";
		}

		return redirect()->to('/producto_codegniter4/public/Productos');
	}

	public function getDatos($id)
	{
		$producto = new Producto();

		$data['productos'] = $producto->getProducto($id);
		$data['marca'] =  $producto->getMarca();
		$data['proveedor'] = $producto->getProveedor();
		$data['categoria'] = $producto->getCategoria();
		echo view('templates/header', $data);
		echo view('producto/form');
		echo view('templates/footer');
	}




	




	//-------------------------------------------------------------------
	//--------------------------------------------------------------------	
}